package com.hopur21.Utils;

import java.util.*;
import java.util.stream.IntStream;

public class Combinations<T> {

    final private List<List<T>> result = new ArrayList<>();

    public Combinations(List<T> items, int numSubLists) {
        int[] indexes = IntStream.range(0, numSubLists).toArray();

        while (indexes[0] < (items.size() - numSubLists + 1)) {
            List<T> v = new ArrayList<>();
            for (int i = 0; i < numSubLists; i++) {
                v.add(items.get(indexes[i]));
            }
            result.add(v);
            indexes[numSubLists - 1]++;
            int l = numSubLists - 1;
            while ((indexes[numSubLists - 1] >= items.size()) && (indexes[0] < items.size() - numSubLists + 1)) {
                l--;
                indexes[l]++;
                for (int i = l + 1; i < numSubLists; i++) {
                    indexes[i] = indexes[l] + (i + l);
                }
            }
        }
    }

    public Collection<List<T>> getResult() {
        return result;
    }

    public static void main(String[] args) {
        List<Integer> integers = new ArrayList<>();
        for (int i = 0; i < 60; i++) {
            integers.add(i);
        }
        Combinations<Integer> combinations = new Combinations<>(integers, 2);
        combinations.getResult().forEach(System.out::println);
    }

}