package com.hopur21.Solvers;

import com.hopur21.Exceptions.NoSolutionFoundException;
import com.hopur21.Models.Course;
import com.hopur21.Models.Timetable;
import io.jenetics.*;
import io.jenetics.engine.Engine;
import io.jenetics.engine.EvolutionResult;
import io.jenetics.engine.EvolutionStatistics;
import io.jenetics.engine.Limits;
import io.jenetics.util.Factory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;
import java.util.stream.Collectors;

public class SecondGeneticSolver extends Solver {

    private static int[] bitLengths;
    private static int totalBitLength;
    private static List<Course> courses;

    private static double eval(Genotype<BitGene> gt) {
        double score = 0.0;

        BitSet bitSet = gt.get(0).as(BitChromosome.class).toBitSet();
        int cumulativeBits = 0;
        for (int i = 0; i < bitLengths.length; i += 2) {
            BitSet roomOptions = bitSet.get(cumulativeBits, cumulativeBits + bitLengths[i]);
            cumulativeBits += bitLengths[i];
            BitSet timeOptions = bitSet.get(cumulativeBits, cumulativeBits + bitLengths[i+1]);
            cumulativeBits += bitLengths[i+1];
            Course course = courses.get(i / 2);

            int[] roomAssignments;
            if (course.rooms.size() == 1)
                roomAssignments = new int[] {0};
            else
                roomAssignments = roomOptions.stream()
                        .limit(course.nrRooms)
                        .toArray();

            int timeAssignment = timeOptions.stream().findFirst().orElse(0);

            score += Arrays.stream(roomAssignments)
                    .mapToDouble(index -> course.rooms.get(index).getPreferenceValue())
                    .sum();
            if (roomAssignments.length != course.nrRooms) {
                score += 1000;
            }

            score += course.times.get(timeAssignment).preference;
        }
        return score;
    }

    public SecondGeneticSolver(Timetable timetable) {
        super(timetable);

        courses = new ArrayList<>(timetable.courses.values());

        bitLengths = new int[timetable.courses.size() * 2];
        totalBitLength = 0;

        int i = 0;
        for (Course course : courses) {

            bitLengths[i] = course.rooms.size();
            bitLengths[i+1] = course.times.size();

            totalBitLength += bitLengths[i] + bitLengths[i+1];

            i += 2;
        }

        Factory<Genotype<BitGene>> gtf = Genotype.of(BitChromosome.of(totalBitLength, 0.8));

        Engine<BitGene, Double> engine = Engine
                .builder(SecondGeneticSolver::eval, gtf)
                .minimizing()
                .survivorsSelector(new TournamentSelector<>(5))
                .offspringSelector(new RouletteWheelSelector<>())
                .alterers(
                        new Mutator<>(0.115),
                        new SinglePointCrossover<>(0.16))
                .build();

        final EvolutionStatistics<Double, ?> statistics = EvolutionStatistics.ofNumber();

        Genotype<BitGene> result = engine.stream()
                .limit(Limits.bySteadyFitness(7))
                .limit(1500)
                .peek(statistics)
                .collect(EvolutionResult.toBestGenotype());

        System.out.println(statistics);
        System.out.println("Genetic Solution:\n" + result);
    }

    @Override
    public Solution solve() throws NoSolutionFoundException {
        return null;
    }

}
