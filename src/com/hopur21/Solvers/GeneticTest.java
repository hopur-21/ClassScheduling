package com.hopur21.Solvers;

import com.hopur21.Models.Course;
import com.hopur21.Models.Timetable;
import com.hopur21.Parsers.Parser;
import com.hopur21.Parsers.UniTime;
import io.jenetics.*;
import io.jenetics.engine.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import static io.jenetics.engine.EvolutionResult.toBestPhenotype;
import static io.jenetics.engine.Limits.bySteadyFitness;

public class GeneticTest implements Problem<GeneticTest.Assignment[], BitGene, Double> {
    final Timetable timetable;
    final Random random = new Random(123);
    int maxRoomOptionSize;
    int maxTimeOptionsSize;

    @Override
    public Function<Assignment[], Double> fitness() {
        return assignments -> Arrays.stream(assignments).mapToDouble(Assignment::fitness).sum();
    }

    @Override
    public Codec<Assignment[], BitGene> codec() {
        return Codec.of(
                Genotype.of(
                        BitChromosome.of(maxRoomOptionSize, 0.25),
                        BitChromosome.of(maxTimeOptionsSize, 0.25)
                ),
                gt -> {
                    int[] roomAssignments = gt.get(0).as(BitChromosome.class).ones().toArray();
                    int timeAssignment = gt.get(0).as(BitChromosome.class).ones().findFirst().orElse(0);
                    return timetable.courses.values().stream()
                            .map(c -> new Assignment(roomAssignments, timeAssignment, c))
                            .toArray(Assignment[]::new);
                }
        );
    }

    public GeneticTest(Timetable timetable) {
        this.timetable = timetable;

        maxRoomOptionSize = timetable.courses.values().stream()
                .mapToInt(c -> c.rooms.size())
                .max()
                .orElse(0);
        maxTimeOptionsSize = timetable.courses.values().stream()
                .mapToInt(c -> c.times.size())
                .max()
                .orElse(0);
    }

    public static void main(final String[] args) {
        Timetable timetable;
        try {
            Parser parser = new UniTime("data/pu-fal07-llr-cs.xml");
            timetable = parser.parse();
        } catch (UniTime.ParserException | IOException | SAXException | ParserConfigurationException e) {
            e.printStackTrace();
            return;
        }


        final GeneticTest geneticTest = new GeneticTest(timetable);
        System.out.println("Created solution blueprint");

        // Configure and build the evolution engine.
        final Engine<BitGene, Double> engine = Engine
                .builder(geneticTest)
                .minimizing()
                .build();

        // Create evolution statistics consumer.
        final EvolutionStatistics<Double, ?>
                statistics = EvolutionStatistics.ofNumber();

        final Assignment[] result = geneticTest.codec().decoder().apply(
                engine.stream()
                    .limit(100)
                .collect(EvolutionResult.toBestGenotype())
        );

        final Phenotype<BitGene, Double> best = engine.stream()
                // Truncate the evolution stream after 7 "steady"
                // generations.
                .limit(bySteadyFitness(7))
                // The evolution will stop after maximal 100
                // generations.
                .limit(100)
                // Update the evaluation statistics after
                // each generation
                .peek(statistics)
                // Collect (reduce) the evolution stream to
                // its best phenotype.
                .collect(toBestPhenotype());

        System.out.println(statistics);
        System.out.println(best);
        System.out.println(Arrays.toString(geneticTest.codec().decode(best.getGenotype())));
    }

    public static final class Assignment implements Serializable {
        private static final long serialVersionUID = 1L;

        private int[] roomAssignments;
        private int timeAssignment;
        private Course course;

        public Assignment(int[] roomAssignments, int timeAssignment, Course course) {
            if (course.rooms.size() > 0) {
                // truncate roomAssignments to the valid array size
                this.roomAssignments = Arrays.stream(roomAssignments)
                        .filter(i -> i < course.rooms.size())
                        .limit(course.nrRooms)
                        .toArray();
            }
            if (course.times.size() > 0) {
                // time assignment must be within times size
                this.timeAssignment = timeAssignment % course.times.size();
            }
            this.course = course;
        }

        public int[] getRoomAssignments() {
            return roomAssignments;
        }

        public int getTimeAssignment() {
            return timeAssignment;
        }

        public static AtomicInteger createdCount = new AtomicInteger(0);

        public double fitness() {

            double sum = 0;

            if (roomAssignments != null) {
                sum += Arrays.stream(roomAssignments)
                        .mapToDouble(i -> course.rooms.get(i).getPreferenceValue())
                        .sum();

                if (roomAssignments.length != course.nrRooms) {
                    sum += 1000;
                }
            }

            sum += course.times.get(timeAssignment).preference;

            return sum;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Assignment that = (Assignment) o;
            return timeAssignment == that.timeAssignment &&
                    Arrays.equals(roomAssignments, that.roomAssignments);
        }

        @Override
        public int hashCode() {
            int result = Objects.hash(timeAssignment);
            result = 31 * result + Arrays.hashCode(roomAssignments);
            return result;
        }

        @Override
        public String toString() {
            return "Assignment{" +
                    "roomAssignments=" + Arrays.toString(roomAssignments) +
                    ", timeAssignment=" + timeAssignment +
                    '}';
        }
    }
}
