package com.hopur21.Solvers;

import com.hopur21.Exceptions.AlreadyAssignedException;
import com.hopur21.Exceptions.RoomOccupiedException;
import com.hopur21.Models.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class Solution {
    final private Timetable timetable;

    private HashMap<Room, Collection<CourseAssignment>> roomAssignments;
    private HashMap<Course, CourseAssignment> courseAssignments;
    private HashMap<Time, CourseAssignment> timeAssignment;

    private double roomPreferenceSum = 0.0;
    private double timePreferenceSum = 0.0;

    public Solution(Timetable timetable) {
        this.timetable = timetable;

        roomAssignments = new HashMap<>();
        courseAssignments = new HashMap<>();
        timeAssignment = new HashMap<>();
    }

    public Solution(Solution solution) {
        this.timetable = solution.timetable;

        roomAssignments = new HashMap<>(solution.roomAssignments);
        courseAssignments = new HashMap<>(solution.courseAssignments);
        timeAssignment = new HashMap<>(solution.timeAssignment);
    }

    private boolean canBeAssigned(CourseAssignment courseAssignment) {
        Collection<CourseAssignment> conflictingCourses = roomAssignments.get(courseAssignment.rooms.getLast())
                .stream()
                .filter(courseAssignment::intersects)
                .collect(Collectors.toList());
        if (conflictingCourses.size() == 0) {
            return true;
        }

        Collection<Constraint> constraints = timetable.courseConstraints.get(courseAssignment.course);
        return constraints != null && constraints
                .stream()
                .allMatch(constraint ->
                        constraint.type == ConstraintTypes.CAN_SHARE_ROOM || constraint.type == ConstraintTypes.MEET_WITH);
    }

    /**
     * Assigns a course to a room and a time
     *
     * @param course the course to assign to the room
     * @param room   the room to assign the course to
     * @param time   the time to assign the course to
     * @throws AlreadyAssignedException throws when course has already been assigned all needed rooms
     * @throws RoomOccupiedException    throws when the room is already occupied at that time
     */
    public void assignCourse(Course course, Room room, Time time) throws AlreadyAssignedException, RoomOccupiedException {
        if (course == null || room == null || time == null)
            throw new IllegalArgumentException("room, course and time must all be non-null values");

        Collection<CourseAssignment> assignments = roomAssignments.putIfAbsent(room, new ArrayList<>());
        if (assignments == null)
            assignments = roomAssignments.get(room);

        CourseAssignment courseAssignment = new CourseAssignment(course, room, time);

        if (canBeAssigned(courseAssignment)) {
            // TODO: add preference values to preference sums
            courseAssignment.rooms.forEach(r -> roomAssignments.get(r).add(courseAssignment));
            courseAssignments.put(courseAssignment.course, courseAssignment);
            timeAssignment.put(courseAssignment.time, courseAssignment);
        } else {
            // Find conflicting time
            CourseAssignment conflictingAssignment = roomAssignments.get(courseAssignment.rooms.getLast())
                    .stream()
                    .filter(ca -> !ca.intersects(courseAssignment.time))
                    .findFirst()
                    .orElse(null);
            if (conflictingAssignment == null)
                throw new RoomOccupiedException("room already occupied");
            throw new RoomOccupiedException("Room " +
                    courseAssignment.rooms.getLast().id +
                    " is already occupied at " +
                    courseAssignment.time +
                    " conflicting with " +
                    conflictingAssignment.time +
                    " conflicting courses are " +
                    courseAssignment.course.id + " and " +
                    conflictingAssignment.course.id
            );
        }
    }

    public void assignCourse(Course course, List<Room> rooms, Time time) throws AlreadyAssignedException, RoomOccupiedException {
        if (course == null || rooms == null || time == null)
            throw new IllegalArgumentException("room, course and time must all be non-null values");

        Solution newSolution = new Solution(this);

        for (Room room :
                rooms) {
            newSolution.assignCourse(course, room, time);
        }

        this.roomAssignments = new HashMap<>(newSolution.roomAssignments);
        this.courseAssignments = new HashMap<>(newSolution.courseAssignments);
        this.timeAssignment = new HashMap<>(newSolution.timeAssignment);
    }

    /**
     * Evaluates the solution, smaller is better
     *
     * @return evaluation value
     */
    public double evaluate() {
        double evaluation = 0;

        // Add number of unassigned courses to evaluation, ignore courses that dont need rooms
        evaluation += timetable.courses.values()
                .stream()
                .filter(c -> c.nrRooms > 0).count() - this.courseAssignments.size();
        
        // Add room preferences to evaluation
        evaluation += roomPreferenceSum;

        // Add time preferences to evaluation
        evaluation += timePreferenceSum;
        
        return evaluation;
    }
    
    public boolean isComplete() {
        long nrToAssign = timetable.courses.values()
                .stream()
                .filter(c -> c.nrRooms > 0).count();

        if (nrToAssign == this.courseAssignments.size()) {
        	return true;
        }
        
        return false;  
    }
}
