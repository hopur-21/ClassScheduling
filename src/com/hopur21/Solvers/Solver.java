package com.hopur21.Solvers;

import com.hopur21.Exceptions.NoSolutionFoundException;
import com.hopur21.Models.Timetable;

public abstract class Solver {
    protected Timetable timetable;

    public Solver(Timetable timetable) {
        this.timetable = timetable;
    }

    abstract public Solution solve() throws NoSolutionFoundException;
}
