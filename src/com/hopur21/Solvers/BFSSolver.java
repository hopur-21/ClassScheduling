package com.hopur21.Solvers;


import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

import com.hopur21.Exceptions.AlreadyAssignedException;
import com.hopur21.Exceptions.NoSolutionFoundException;
import com.hopur21.Exceptions.RoomOccupiedException;
import com.hopur21.Models.Course;
import com.hopur21.Models.Preference;
import com.hopur21.Models.Room;
import com.hopur21.Models.Time;
import com.hopur21.Models.Timetable;
import com.hopur21.Utils.Combinations;


public class BFSSolver extends Solver {

  public BFSSolver(Timetable timetable) {
    super(timetable);
  }

  @Override
  public Solution solve() throws NoSolutionFoundException {
    
    return BFS(new Solution(this.timetable));
  }
  
  public Solution BFS(Solution initialState) {
    Solution completeSolution = null;
    
	Queue<Solution> frontier = new LinkedList<Solution>();
    
    frontier.add(initialState);
    int counter = 0;
    while (!frontier.isEmpty()) {
    	Solution subtree_root = frontier.remove();
    	
    	// Complete solution found
    	if (subtree_root.isComplete()) {
    		completeSolution = subtree_root;
    		break;
    	}
    	
    	if (frontier.size() % 1000 == 0) {
    		System.out.println(frontier.size());
    	}
    	
    	for (Course course :
            	timetable.courses.values().stream().filter(c -> c.nrRooms > 0).collect(Collectors.toList())) {
    		List<Preference<Room>> roomChoices = new ArrayList<>(course.rooms);
    		Deque<Time> timeChoices = new ArrayDeque<>(course.times);
    		Collection<List<Preference<Room>>> roomCombinations = new Combinations<>(roomChoices, course.nrRooms).getResult();
    		
    		System.out.println(frontier.size());
	        for (Time time :
	                timeChoices) {
	            for (List<Preference<Room>> roomCombination :
	                    roomCombinations) {
	                try {
	                    List<Room> rooms = roomCombination
	                            .stream()
	                            .map(r -> timetable.rooms.get(r.getPreferenceObject().id)).collect(Collectors.toList());
	                    
	                    Solution nextSolution = subtree_root;
	                    nextSolution.assignCourse(course, rooms, time);
	                    frontier.add(nextSolution);
	                } catch (AlreadyAssignedException | RoomOccupiedException e) {
	                    //e.printStackTrace();
	                	counter++;
	                	if (counter % 1000 == 0) {
	                		System.out.println("Catch " + counter);
	                	}
	                	
	                }
	            }
	        }
    	}
    	
    }
    
    return completeSolution;
  }

}