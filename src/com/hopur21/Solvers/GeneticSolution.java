package com.hopur21.Solvers;

import com.hopur21.Models.Course;
import com.hopur21.Models.Timetable;
import com.hopur21.Parsers.Parser;
import com.hopur21.Parsers.UniTime;
import io.jenetics.*;
import io.jenetics.engine.*;
import io.jenetics.util.ISeq;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import static io.jenetics.engine.EvolutionResult.toBestPhenotype;
import static io.jenetics.engine.Limits.bySteadyFitness;

public class GeneticSolution implements Problem<ISeq<GeneticSolution.Assignment>, BitGene, Double> {
    private final Codec<ISeq<Assignment>, BitGene> _codec;

    public GeneticSolution(final ISeq<Assignment> assignments) {
        _codec = Codecs.ofSubSet(assignments);
    }

    public static GeneticSolution of(Collection<Course> courses, final Random random) {
        return new GeneticSolution(
                courses.stream()
                        .filter(c -> c.nrRooms > 0)
                        .map(c -> Assignment.fromCourse(c, random))
                        .collect(ISeq.toISeq())
        );
    }

    @Override
    public Function<ISeq<Assignment>, Double> fitness() {
        return assignments -> {
            return assignments.stream().mapToDouble(a -> {
                double sum = Arrays.stream(a.roomAssignments)
                        .mapToDouble(i -> a.course.rooms.get(i).getPreferenceValue()).sum();
                sum += a.course.times.get(a.timeAssignment).preference;
                return sum;
            }).sum();
        };
    }

    @Override
    public Codec<ISeq<Assignment>, BitGene> codec() {
        return _codec;
    }

    public static final class Assignment implements Serializable {
        private static final long serialVersionUID = 1L;

        private int[] roomAssignments;
        private int timeAssignment;
        private Course course;

        public Assignment(int[] roomAssignments, int timeAssignment, Course course) {
            this.roomAssignments = roomAssignments;
            this.timeAssignment = timeAssignment;
            this.course = course;
        }

        public int[] getRoomAssignments() {
            return roomAssignments;
        }

        public int getTimeAssignment() {
            return timeAssignment;
        }

        public static AtomicInteger createdCount = new AtomicInteger(0);

        public static Assignment fromCourse(final Course course, final Random random) {
            createdCount.incrementAndGet();
            return new Assignment(
                    random.ints(0, course.rooms.size()).limit(course.rooms.size()).toArray(),
                    random.nextInt(course.times.size()), course);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Assignment that = (Assignment) o;
            return timeAssignment == that.timeAssignment &&
                    Arrays.equals(roomAssignments, that.roomAssignments);
        }

        @Override
        public int hashCode() {
            int result = Objects.hash(timeAssignment);
            result = 31 * result + Arrays.hashCode(roomAssignments);
            return result;
        }

        @Override
        public String toString() {
            return "Assignment{" +
                    "roomAssignments=" + Arrays.toString(roomAssignments) +
                    ", timeAssignment=" + timeAssignment +
                    '}';
        }
    }

    public static void main(final String[] args) {
        Timetable timetable;
        try {
            Parser parser = new UniTime("data/pu-fal07-llr-cs.xml");
            timetable = parser.parse();
        } catch (UniTime.ParserException | IOException | SAXException | ParserConfigurationException e) {
            e.printStackTrace();
            return;
        }

        final GeneticSolution solution = GeneticSolution.of(timetable.courses.values(), new Random(123));
        System.out.println("Created solution blueprint");

        // Configure and build the evolution engine.
        final Engine<BitGene, Double> engine = Engine.builder(solution)
                .populationSize(500)
                .minimizing()
                .survivorsSelector(new TournamentSelector<>(5))
                .offspringSelector(new RouletteWheelSelector<>())
                .alterers(
                        new Mutator<>(0.115),
                        new SinglePointCrossover<>(0.16))
                .build();

        // Create evolution statistics consumer.
        final EvolutionStatistics<Double, ?>
                statistics = EvolutionStatistics.ofNumber();

        final Phenotype<BitGene, Double> best = engine.stream()
                // Truncate the evolution stream after 7 "steady"
                // generations.
                .limit(bySteadyFitness(7))
                // The evolution will stop after maximal 100
                // generations.
                .limit(100)
                // Update the evaluation statistics after
                // each generation
                .peek(statistics)
                // Collect (reduce) the evolution stream to
                // its best phenotype.
                .collect(toBestPhenotype());

        System.out.println(statistics);
        System.out.println(best);
    }
}
