package com.hopur21.Solvers;

import com.hopur21.Exceptions.AlreadyAssignedException;
import com.hopur21.Exceptions.NoSolutionFoundException;
import com.hopur21.Exceptions.RoomOccupiedException;
import com.hopur21.Models.*;
import com.hopur21.Utils.Combinations;

import java.util.*;
import java.util.stream.Collectors;

public class PogoSolver extends Solver {
    private int tested = 0;

    public PogoSolver(Timetable timetable) {
        super(timetable);
    }

    @Override
    public Solution solve() throws NoSolutionFoundException {
        Solution solution = new Solution(this.timetable);

        for (Course course :
                timetable.courses.values().stream().filter(c -> c.nrRooms > 0).collect(Collectors.toList())) {
            List<Preference<Room>> roomChoices = new ArrayList<>(course.rooms);
            Deque<Time> timeChoices = new ArrayDeque<>(course.times);
            Collection<List<Preference<Room>>> roomCombinations = new Combinations<>(roomChoices, course.nrRooms).getResult();

            timeLoop:
            for (Time time :
                    timeChoices) {
                for (List<Preference<Room>> roomCombination :
                        roomCombinations) {
                    try {
                        List<Room> rooms = roomCombination
                                .stream()
                                .map(r -> timetable.rooms.get(r.getPreferenceObject().id)).collect(Collectors.toList());
                        solution.assignCourse(course, rooms, time);
                        break timeLoop;
                    } catch (AlreadyAssignedException | RoomOccupiedException e) {
                        //e.printStackTrace();
                    }
                }
            }
        }
        return solution;
    }
}
