package com.hopur21.Models;

public class SharingPattern {
    final String pattern;
    final int unit;

    public SharingPattern(String pattern, int unit) {
        this.pattern = pattern;
        this.unit = unit;
    }
}
