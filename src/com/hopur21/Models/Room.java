package com.hopur21.Models;

import java.util.Objects;

public class Room {
    final public int id;
    final public int capacity;
    final public Location location;

    final public boolean ignoreTooFar;
    final public boolean constraint;
    final public boolean discouraged;

    final public Sharing sharing;

    public Room(int id, int capacity, Location location, boolean ignoreTooFar, boolean constraint, boolean discouraged, Sharing sharing) {
        this.id = id;
        this.capacity = capacity;
        this.location = location;
        this.ignoreTooFar = ignoreTooFar;
        this.constraint = constraint;
        this.discouraged = discouraged;
        this.sharing = sharing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return id == room.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
