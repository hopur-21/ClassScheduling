package com.hopur21.Models;

public class Preference<T> implements Comparable<Preference> {
    private final double preferenceValue;
    private final T preferenceObject;

    public Preference(double preferenceValue, T preferenceObject) {
        this.preferenceValue = preferenceValue;
        this.preferenceObject = preferenceObject;
    }

    public double getPreferenceValue() {
        return preferenceValue;
    }

    public T getPreferenceObject() {
        return preferenceObject;
    }

    @Override
    public int compareTo(Preference preference) {
        return -Double.compare(this.preferenceValue, preference.preferenceValue);
    }
}
