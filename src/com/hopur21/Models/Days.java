package com.hopur21.Models;

import java.util.BitSet;

public class Days {
    final private String raw;
    final private BitSet bitSet;

    public Days(String raw) {
        this.raw = raw;
        bitSet = new BitSet(raw.length());
        for (int i = 0; i < raw.length(); i++) {
            if (raw.charAt(i) == '1')
                bitSet.set(i);
        }
    }

    public boolean intersects(Days d) {
        return d.bitSet.intersects(this.bitSet);
    }

    @Override
    public String toString() {
        return "Days{" +
                "raw='" + raw + '\'' +
                '}';
    }
}
