package com.hopur21.Models;

import java.util.regex.Pattern;

public enum ConstraintTypes {
    CAN_SHARE_ROOM,     // (Can Share Room)
    MEET_WITH;          // (SAME AS CAN SHARE ROOM FFS)

    private Integer value;

    ConstraintTypes(Integer value) {
        this.value = value;
    }

    ConstraintTypes() {
        this.value = null;
    }

    public static ConstraintTypes parseConstraintType(String s) {
        String[] partitioned = s.split(Pattern.quote("("));
        if (partitioned.length == 1)
            return ConstraintTypes.valueOf(s);
        return ConstraintTypes.valueOf(partitioned[0]);
    }
}
