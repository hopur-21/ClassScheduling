package com.hopur21.Models;

import java.util.List;
import java.util.Objects;

public class Course {
    final public int id;

    final public Integer offering;
    final public Integer config;
    final public Integer subpart;
    final public Integer parent;
    final public Integer scheduler;
    final public Integer department;
    final public Integer classLimit;

    final public boolean committed;

    final public int minClassLimit = 0;
    final public int maxClassLimit = 0;
    final public double roomToLimitRatio = 1.0;
    final public int nrRooms;

    final public List<Instructor> instructor;
    final public List<Preference<Room>> rooms;
    final public List<Time> times;

    private int studentCount = 0;

    public int getStudentCount() {
        return studentCount;
    }

    public void setStudentCount(int studentCount) {
        this.studentCount = studentCount;
    }

    public void incrementStudentCount() {
        studentCount++;
    }

    public Course(int id, List<Instructor> instructor, Integer offering, Integer config, Integer subpart, Integer parent, Integer scheduler, Integer department, boolean committed, Integer classLimit, int nrRooms, List<Preference<Room>> rooms, List<Time> times) {
        this.id = id;
        this.instructor = instructor;
        this.offering = offering;
        this.config = config;
        this.subpart = subpart;
        this.parent = parent;
        this.scheduler = scheduler;
        this.department = department;
        this.committed = committed;
        this.classLimit = classLimit;
        this.nrRooms = nrRooms;
        this.rooms = rooms;
        this.times = times;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", offering=" + offering +
                ", config=" + config +
                ", subpart=" + subpart +
                ", parent=" + parent +
                ", scheduler=" + scheduler +
                ", department=" + department +
                ", classLimit=" + classLimit +
                ", committed=" + committed +
                ", minClassLimit=" + minClassLimit +
                ", maxClassLimit=" + maxClassLimit +
                ", roomToLimitRatio=" + roomToLimitRatio +
                ", nrRooms=" + nrRooms +
                ", instructor=" + instructor +
                ", rooms=" + rooms +
                ", times=" + times +
                ", studentCount=" + studentCount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course aCourse = (Course) o;
        return id == aCourse.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
