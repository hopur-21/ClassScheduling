package com.hopur21.Models;

public class OfferingsWeight {
    final int id;
    final double weight;

    public OfferingsWeight(int id, double weight) {
        this.id = id;
        this.weight = weight;
    }
}
