package com.hopur21.Models;

public class SharingDepartment {
    final int value;
    final int id;

    public SharingDepartment(int value, int id) {
        this.value = value;
        this.id = id;
    }
}
