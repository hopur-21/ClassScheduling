package com.hopur21.Models;

import java.util.List;

public class Student {
    public final int id;

    public final List<Course> enrolledCourses;
    public final List<Course> prohibitedCourses;

    public final List<OfferingsWeight> offerings;

    public Student(int id, List<Course> enrolledCourses, List<Course> prohibitedCourses, List<OfferingsWeight> offerings) {
        this.id = id;
        this.enrolledCourses = enrolledCourses;
        this.prohibitedCourses = prohibitedCourses;
        this.offerings = offerings;
    }
}
