package com.hopur21.Models;

public enum ConstraintLevel {
    REQUIRED(3),
    STRONGLY_PREFERRED(-2),
    PREFERRED(-1),

    DISCOURAGED(1),
    STRONGLY_DISCOURAGED(2),
    PROHIBITED(-3);

    private Integer value;

    ConstraintLevel(Integer value) {
        this.value = value;
    }
}
