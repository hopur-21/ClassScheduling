package com.hopur21.Models;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Constraint {
    final public UUID uuid = UUID.randomUUID();
    final public int id;
    final public ConstraintTypes type;
    final public int courseLimit = 0;
    final public int delta = 0;
    final public List<Course> affectedCourses = new ArrayList<>();

    public Constraint(int id, ConstraintTypes type) {
        this.id = id;
        this.type = type;
    }
}
