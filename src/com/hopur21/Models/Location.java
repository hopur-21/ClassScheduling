package com.hopur21.Models;

import java.util.Arrays;

public class Location {
    final int x;
    final int y;

    public Location(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double distanceBetween(Location l) {
        return 10 * (Math.pow(l.x - x, 2) + Math.pow(l.y, 2)) / 2;
    }

    public static Location parseLocation(String s) {
        int[] xy = Arrays.stream(s.split(",")).mapToInt(Integer::parseInt).toArray();
        return new Location(xy[0], xy[1]);
    }
}
