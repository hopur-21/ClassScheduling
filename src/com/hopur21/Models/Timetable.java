package com.hopur21.Models;

import java.util.*;

public class Timetable {
    final public String version;
    final public String initiative;
    final public String term;
    final public String created;
    final public int numberOfDays;
    final public int slotsPerDay;

    final public Map<Integer, Room> rooms = new HashMap<Integer, Room>();
    final public Map<Integer, Course> courses = new HashMap<>();
    final public Map<UUID, Constraint> constraints = new HashMap<UUID, Constraint>();
    final public Map<Integer, Student> students = new HashMap<Integer, Student>();

    final public Map<Course, Collection<Constraint>> courseConstraints = new HashMap<>();

    private int semesterLength;

    public int getSemesterLength() {
        return semesterLength;
    }

    public void setSemesterLength(int semesterLength) {
        this.semesterLength = semesterLength;
    }

    public Timetable(String version, String initiative, String term, String created, int numberOfDays, int slotsPerDay) {
        this.version = version;
        this.initiative = initiative;
        this.term = term;
        this.created = created;
        this.numberOfDays = numberOfDays;
        this.slotsPerDay = slotsPerDay;
    }

    @Override
    public String toString() {
        return "Timetable{" +
                "version='" + version + '\'' +
                ", initiative='" + initiative + '\'' +
                ", term='" + term + '\'' +
                ", created='" + created + '\'' +
                ", numberOfDays=" + numberOfDays +
                ", slotsPerDay=" + slotsPerDay +
                ", room count=" + rooms.size() +
                ", course count=" + courses.size() +
                ", constraint cont=" + constraints.size() +
                ", student count=" + students.size() +
                '}';
    }
}
