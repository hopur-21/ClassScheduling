package com.hopur21.Models;

import java.util.ArrayDeque;
import java.util.Deque;

public class CourseAssignment {
    final public Course course;
    final public Deque<Room> rooms;
    final public Time time;

    public CourseAssignment(Course course, Room room, Time time) {
        this.course = course;
        this.rooms = new ArrayDeque<>();
        this.rooms.add(room);
        this.time = time;
    }

    public CourseAssignment(Course course, Deque<Room> rooms, Time time) {
        this.course = course;
        this.rooms = rooms;
        this.time = time;
    }

    public boolean intersects(CourseAssignment courseAssignment) {
        return time.intersects(courseAssignment.time);
    }

    public boolean intersects(Time time) {
        return this.time.intersects(time);
    }
}
