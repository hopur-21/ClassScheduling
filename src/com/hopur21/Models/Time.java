package com.hopur21.Models;

import java.util.Objects;

public class Time extends OldPreference {
    final public Dates dates;
    final public Days days;
    final public int startSlot;
    final public int length;

    public Time(Dates dates, Days days, int startSlot, int length, double preference) {
        super(0, preference);
        this.dates = dates;
        this.days = days;
        this.startSlot = startSlot;
        this.length = length;
    }

    public boolean intersects(Time t) {
        return dates.intersects(t.dates) && days.intersects(t.days) && ((this.startSlot + this.length) > t.startSlot) && ((t.startSlot + t.length) > this.startSlot);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Time time = (Time) o;
        return startSlot == time.startSlot &&
                length == time.length &&
                Objects.equals(dates, time.dates) &&
                Objects.equals(days, time.days);
    }

    @Override
    public int hashCode() {

        return Objects.hash(dates, days, startSlot, length);
    }

    @Override
    public String toString() {
        return "Time{" +
                "dates=" + dates +
                ", days=" + days +
                ", startSlot=" + startSlot +
                ", length=" + length +
                '}';
    }
}
