package com.hopur21.Models;

public abstract class OldPreference implements Comparable<OldPreference> {
    final public int id;
    final public double preference;

    public OldPreference(int id, double preference) {
        this.id = id;
        this.preference = preference;
    }

    @Override
    public int compareTo(OldPreference p) {
        return -Double.compare(preference, p.preference);
    }
}
