package com.hopur21.Models;

import java.util.Collection;

public class Sharing {
    final public SharingPattern pattern;
    final public char freeForAll;
    final public char notAvailable;
    final public Collection<SharingDepartment> department;

    public Sharing(SharingPattern pattern, char freeForAll, char notAvailable, Collection<SharingDepartment> department) {
        this.pattern = pattern;
        this.freeForAll = freeForAll;
        this.notAvailable = notAvailable;
        this.department = department;
    }
}
