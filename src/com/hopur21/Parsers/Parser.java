package com.hopur21.Parsers;

import com.hopur21.Models.Timetable;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public interface Parser {
    Timetable parse() throws UniTime.ParserException, ParserConfigurationException, IOException, SAXException;
}
