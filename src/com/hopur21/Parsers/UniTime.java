package com.hopur21.Parsers;

import com.hopur21.Models.*;
import com.hopur21.Models.Course;
import com.hopur21.Models.Location;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UniTime implements Parser {
    @Override
    public Timetable parse() throws ParserException, ParserConfigurationException, IOException, SAXException {
        File file = new File(filename);
        DocumentBuilder documentBuilder;
        Document document;
        documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        document = documentBuilder.parse(file);
        parseTimetable(document.getDocumentElement());
        return timetable;
    }

    public class ParserException extends Exception {}

    final private String filename;
    private Timetable timetable;

    public UniTime(String filename) {
        this.filename = filename;
    }

    private Timetable parseTimetable(Element element) throws ParserException {
        timetable = new Timetable(
                element.getAttribute("version"),
                element.getAttribute("initiative"),
                element.getAttribute("term"),
                element.getAttribute("created"),
                Integer.parseInt(element.getAttribute("nrDays")),
                Integer.parseInt(element.getAttribute("slotsPerDay")));

        // Parse all child nodes
        NodeList childNodes = element.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            // Only handle element nodes
            Node node = childNodes.item(i);
            if (node.getNodeType() != Node.ELEMENT_NODE)
                continue;

            Element childElement = (Element) node;

            switch (childElement.getNodeName()) {
                case "rooms":
                    parseRooms(childElement).forEach(x -> timetable.rooms.put(x.id, x));
                    System.out.println("Found " + timetable.rooms.size() + " rooms");
                    break;
                case "classes":
                    parseClasses(childElement).forEach(x -> timetable.courses.put(x.id, x));
                    System.out.println("Found " + timetable.courses.size() + " courses");
                    break;
                case "groupConstraints":
                    parseConstraints(childElement).forEach(x -> timetable.constraints.put(x.uuid, x));
                    System.out.println("Found " + timetable.constraints.size() + " constraints");
                    break;
                case "students":
                    parseStudents(childElement).forEach(x -> timetable.students.put(x.id, x));
                    System.out.println("Found " + timetable.students.size() + " students");
                    break;
            }
        }

        return timetable;
    }

    private List<Room> parseRooms(Element element) throws ParserException {
        List<Room> rooms = new ArrayList<>();

        NodeList childNodes = element.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            // Only handle element nodes
            Node node = childNodes.item(i);
            if (node != null && node.getNodeType() != Node.ELEMENT_NODE)
                continue;

            Element childElement = (Element) node;

            assert childElement != null;
            if (!childElement.getNodeName().equals("room")) {
                throw new ParserException();
            }

            Sharing sharing = null;
            if (childElement.hasChildNodes()) {
                sharing = parseSharing(childElement);
            }

            rooms.add(new Room(
                    Integer.parseInt(childElement.getAttribute("id")),
                    Integer.parseInt(childElement.getAttribute("capacity")),
                    Location.parseLocation(childElement.getAttribute("location")),
                    Boolean.parseBoolean(childElement.getAttribute("ignoreTooFar")),
                    Boolean.parseBoolean(childElement.getAttribute("constraint")),
                    Boolean.parseBoolean(childElement.getAttribute("discouraged")),
                    sharing
            ));
        }

        return rooms;
    }

    private Sharing parseSharing(Element element) {
        Element patternElement = (Element) element.getElementsByTagName("pattern").item(0);
        SharingPattern sharingPattern = new SharingPattern(
                patternElement.getTextContent(),
                Integer.parseInt(patternElement.getAttribute("unit")));

        Element freeForAllElement = (Element) element.getElementsByTagName("freeForAll").item(0);
        char freeForAll = freeForAllElement.getAttribute("value").charAt(0);

        Element notAvailableElement = (Element) element.getElementsByTagName("notAvailable").item(0);
        char notAvailable = notAvailableElement.getAttribute("value").charAt(0);

        List<SharingDepartment> sharingDepartments = new ArrayList<>();
        NodeList departmentNodes = element.getElementsByTagName("department");
        for (int i = 0; i < departmentNodes.getLength(); i++) {
            Element departmentElement = (Element) departmentNodes.item(i);
            sharingDepartments.add(new SharingDepartment(
                    Integer.parseInt(departmentElement.getAttribute("id")),
                    Integer.parseInt(departmentElement.getAttribute("value"))
            ));
        }

        return new Sharing(sharingPattern, freeForAll, notAvailable, sharingDepartments);
    }

    private List<Course> parseClasses(Element element) throws ParserException {
        List<Course> courses = new ArrayList<>();

        int longestDateValue = Integer.MIN_VALUE;

        NodeList childNodes = element.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            // Only handle element nodes
            Node node = childNodes.item(i);
            if (node != null && node.getNodeType() != Node.ELEMENT_NODE)
                continue;

            Element childElement = (Element) node;

            assert childElement != null;
            if (!childElement.getNodeName().equals("class")) {
                throw new ParserException();
            }

            // Ignore solutions
            if (!childElement.getAttribute("solution").equals(""))
                continue;

            List<Instructor> instructors = new ArrayList<>();
            NodeList instructorNodes = childElement.getElementsByTagName("instructor");
            for (int y = 0; y < instructorNodes.getLength(); y++) {
                Element instructorElement = (Element) instructorNodes.item(y);
                instructors.add(new Instructor(
                        Integer.parseInt(instructorElement.getAttribute("id"))
                ));
            }

            List<Preference<Room>> roomPreferences = new ArrayList<>();
            NodeList roomPreferenceNodes = childElement.getElementsByTagName("room");
            for (int y = 0; y < roomPreferenceNodes.getLength(); y++) {
                Element roomPreferenceElement = (Element) roomPreferenceNodes.item(y);
                //if (!Boolean.parseBoolean(roomPreferenceElement.getAttribute("solution")))
                //    continue;
                roomPreferences.add(new Preference<>(
                        Double.parseDouble(roomPreferenceElement.getAttribute("pref")),
                        timetable.rooms.get(Integer.parseInt(roomPreferenceElement.getAttribute("id")))
                ));
            }

            List<Time> times = new ArrayList<>();
            NodeList timeNodes = childElement.getElementsByTagName("time");
            Dates dates = new Dates(childElement.getAttribute("dates"));
            for (int y = 0; y < timeNodes.getLength(); y++) {
                Element timeElement = (Element) timeNodes.item(y);
                //if (!Boolean.parseBoolean(timeElement.getAttribute("solution")))
                //    continue;
                times.add(new Time(
                        dates,
                        new Days(timeElement.getAttribute("days")),
                        Integer.parseInt(timeElement.getAttribute("start")),
                        Integer.parseInt(timeElement.getAttribute("length")),
                        Double.parseDouble(timeElement.getAttribute("pref"))
                ));
            }

            int datesLength = childElement.getAttribute("dates").length();
            if (datesLength > longestDateValue)
                longestDateValue = datesLength;

            courses.add(new Course(
                    Integer.parseInt(childElement.getAttribute("id")),
                    instructors,
                    toInteger(childElement.getAttribute("offering")),
                    toInteger(childElement.getAttribute("config")),
                    toInteger(childElement.getAttribute("subpart")),
                    toInteger(childElement.getAttribute("parent")),
                    toInteger(childElement.getAttribute("scheduler")),
                    toInteger(childElement.getAttribute("department")),
                    Boolean.parseBoolean(childElement.getAttribute("committed")),
                    toInteger(childElement.getAttribute("classLimit")),
                    toInteger(childElement.getAttribute("nrRooms"), 1),
                    roomPreferences,
                    times
            ));
        }

        timetable.setSemesterLength(longestDateValue);

        return courses;
    }

    private List<Constraint> parseConstraints(Element element) {
        List<Constraint> constraints = new ArrayList<>();

        NodeList childNodes = element.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            // Only handle element nodes
            Node node = childNodes.item(i);
            if (node != null && node.getNodeType() != Node.ELEMENT_NODE)
                continue;

            Element childElement = (Element) node;

            assert childElement != null;
            Constraint constraint;
            try {
                constraint = new Constraint(
                        Integer.parseInt(childElement.getAttribute("id")),
                        ConstraintTypes.parseConstraintType(childElement.getAttribute("type")));
                constraints.add(constraint);
            } catch (IllegalArgumentException ex) {
                continue;
            }

            Collection<Course> courses = new ArrayList<>();
            NodeList courseNodes = childElement.getElementsByTagName("class");
            for (int y = 0; y < courseNodes.getLength(); y++) {
                Element courseElement = (Element) courseNodes.item(y);
                courses.add(timetable.courses.get(Integer.parseInt(courseElement.getAttribute("id"))));
            }
            courses.forEach(course -> {
                timetable.courseConstraints.putIfAbsent(course, new ArrayList<>());
                Collection<Constraint> courseConstraints = timetable.courseConstraints.get(course);
                courseConstraints.add(constraint);
                constraint.affectedCourses.add(course);
            });
        }

        return constraints;
    }

    private List<Student> parseStudents(Element element) throws ParserException {
        List<Student> students = new ArrayList<>();

        NodeList childNodes = element.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            // Only handle element nodes
            Node node = childNodes.item(i);
            if (node != null && node.getNodeType() != Node.ELEMENT_NODE)
                continue;

            Element childElement = (Element) node;

            assert childElement != null;
            if (!childElement.getNodeName().equals("student")) {
                throw new ParserException();
            }

            List<OfferingsWeight> offerings = new ArrayList<>();
            NodeList offeringNodes = childElement.getElementsByTagName("offerings");
            for (int y = 0; y < offeringNodes.getLength(); y++) {
                Element offeringElement = (Element) offeringNodes.item(y);
                offerings.add(new OfferingsWeight(
                        Integer.parseInt(offeringElement.getAttribute("id")),
                        Double.parseDouble(offeringElement.getAttribute("weight"))
                ));
            }

            List<Course> prohibitedCourses = new ArrayList<>();
            NodeList prohibitedNodes = childElement.getElementsByTagName("prohibited-courses");
            for (int y = 0; y < prohibitedNodes.getLength(); y++) {
                Element prohibitedElement = (Element) prohibitedNodes.item(y);
                prohibitedCourses.add(timetable.courses.get(Integer.parseInt(prohibitedElement.getAttribute("id"))));
            }

            List<Course> courses = new ArrayList<>();
            NodeList courseNodes = childElement.getElementsByTagName("class");
            for (int y = 0; y < courseNodes.getLength(); y++) {
                Element courseElement = (Element) courseNodes.item(y);
                Course c = timetable.courses.get(Integer.parseInt(courseElement.getAttribute("id")));
                c.incrementStudentCount();
                courses.add(c);
            }

            students.add(new Student(
                    Integer.parseInt(childElement.getAttribute("id")),
                    courses,
                    prohibitedCourses,
                    offerings
            ));
        }

        return students;
    }

    private Integer toInteger(String s) {
        if (s.trim().equals(""))
            return null;
        return Integer.parseInt(s);
    }

    private Integer toInteger(String s, int def) {
        if (s.trim().equals(""))
            return def;
        return Integer.parseInt(s);
    }
}