package com.hopur21;

import com.hopur21.Exceptions.NoSolutionFoundException;
import com.hopur21.Models.*;
import com.hopur21.Parsers.Parser;
import com.hopur21.Parsers.UniTime;
import com.hopur21.Solvers.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        Timetable timetable;
        try {
            Parser parser = new UniTime("data/pu-fal07-llr-cs.xml");
            timetable = parser.parse();
        } catch (UniTime.ParserException | IOException | SAXException | ParserConfigurationException e) {
            e.printStackTrace();
            return;
        }

        Solver solver = new SecondGeneticSolver(timetable);
        try {
            Solution solution = solver.solve();
            //System.out.println("Solution evalution: " + solution.evaluate());
        } catch (NoSolutionFoundException e) {
            e.printStackTrace();
        }
    }
}
