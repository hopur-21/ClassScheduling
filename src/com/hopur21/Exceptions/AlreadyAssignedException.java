package com.hopur21.Exceptions;

public class AlreadyAssignedException extends Exception {
    public AlreadyAssignedException(String message) {
        super(message);
    }
}
