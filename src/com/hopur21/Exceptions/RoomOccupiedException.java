package com.hopur21.Exceptions;

public class RoomOccupiedException extends Exception {
    public RoomOccupiedException(String message) {
        super(message);
    }
}
